package delatega.dz.a9issa;

import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    Button topButton ;
    Button bottomButton ;
    TextView storyText;


    Story currentStory ;
    int indexCurrentStory = 0;
    Story[] lesStories = {
            new Story(R.string.T1_Story,R.string.T1_Ans1,R.string.T1_Ans2,2,1 ),
            new Story(R.string.T2_Story,R.string.T2_Ans1,R.string.T2_Ans2,2,3 ),
            new Story(R.string.T3_Story,R.string.T3_Ans1,R.string.T3_Ans2,4,5 ),
            new Story(R.string.T4_End ),
            new Story(R.string.T5_End ),
            new Story(R.string.T6_End )
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        topButton = (Button) findViewById(R.id.button_top);
        bottomButton = (Button) findViewById(R.id.button_bottom);
        storyText = (TextView) findViewById(R.id.textView_story);

        currentStory = lesStories[indexCurrentStory];
        actualiserAffi();
        topButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goUp();
                actualiserAffi();
            }
        });
        bottomButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goDown();
                actualiserAffi();
            }
        });

    }
    private void goUp(){
   indexCurrentStory = currentStory.getIndexnextByTopButton();
   currentStory = lesStories[indexCurrentStory];

    }
    private void goDown(){
   indexCurrentStory = currentStory.getIndexnextByBottomButton();
   currentStory = lesStories[indexCurrentStory];
    }
    private void actualiserAffi(){
        storyText.setText(currentStory.getIdStory());
        if (currentStory.getIdAnsTopButton()!=0|| currentStory.getIdAnsBottomButton()!=0) {
                topButton.setText(currentStory.getIdAnsTopButton());
            bottomButton.setText(currentStory.getIdAnsBottomButton());
        }else {
            bottomButton.setVisibility(View.GONE);
            topButton.setVisibility(View.GONE);
            MediaPlayer mp = MediaPlayer.create(getApplicationContext(), R.raw.son);
            mp.start();
        }

    }

}