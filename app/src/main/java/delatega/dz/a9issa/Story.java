package delatega.dz.a9issa;

public class Story {
    private int idStory;
    private int IdAnsTopButton;
    private int IdAnsBottomButton;
    private int IndexnextByTopButton;
    private int IndexnextByBottomButton;

    public Story(int idStory, int idAnsTopButton, int idAnsBottomButton, int indexnextByTopButton, int indexnextByBottomButton) {
        this.idStory = idStory;
        IdAnsTopButton = idAnsTopButton;
        IdAnsBottomButton = idAnsBottomButton;
        IndexnextByTopButton = indexnextByTopButton;
        IndexnextByBottomButton = indexnextByBottomButton;
    }

    public Story(int idStory) {
        this.idStory = idStory;
    }

    public int getIdStory() {
        return idStory;
    }

    public void setIdStory(int idStory) {
        this.idStory = idStory;
    }

    public int getIdAnsTopButton() {
        return IdAnsTopButton;
    }

    public void setIdAnsTopButton(int idAnsTopButton) {
        IdAnsTopButton = idAnsTopButton;
    }

    public int getIdAnsBottomButton() {
        return IdAnsBottomButton;
    }

    public void setIdAnsBottomButton(int idAnsBottomButton) {
        IdAnsBottomButton = idAnsBottomButton;
    }

    public int getIndexnextByTopButton() {
        return IndexnextByTopButton;
    }

    public void setIndexnextByTopButton(int indexnextByTopButton) {
        IndexnextByTopButton = indexnextByTopButton;
    }

    public int getIndexnextByBottomButton() {
        return IndexnextByBottomButton;
    }

    public void setIndexnextByBottomButton(int indexnextByBottomButton) {
        IndexnextByBottomButton = indexnextByBottomButton;
    }
}
